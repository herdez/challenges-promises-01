//const { describe } = require('yargs')
const { expect } = require('@jest/globals')
const { delay } = require('../promiseTiming')


describe('delay promise', () => {

    test('should be an entry numeric string', () => {
        const input = "1"
        return delay(0, input).then(data => {
            expect( data ).toBe("1")
        })
        
    })

    test('should get numeric string "2" from data input', () => {
        const input = "1bc"
        
        return delay(3000, input).then(data => {
            expect( data ).toBe("2")
        })
    })

    test('should return a numeric string from entry number', () => {
        const input = 1
        return delay(1000, input).then(data => {
            expect( data ).toBe("2")
        })
    })

    test('should sum values from input data', () => {
        const input = "22mj"
        return delay(0, input).then(data => {
            expect( data ).toBe("23")
            return delay(3000, data).then(data => {
                expect( data ).toBe("24")
            })
        })
    })

    test('should fail the input string with an error', () => {
        const input = "mb"
        return expect(delay(1000, input)).rejects.toMatch('Input is not a numeric value');
    })

    
    test('should fail any input with an error', () => {
        return expect(delay()).rejects.toMatch('Input is not a numeric value');
    })
      
})