# NodeJS Challenges - Promises

## Objectives:


- How to create a promise.
- How to consume a promise.
- How to chain promises.

### Theory & Documentation

# Promises

## A Promise in Layman Terms

Check this conversation between two people:

- `Wendy`: Hey Mr. Promise! Can you run to the store down the street and get me 
  sauce for this chilaquiles we are cooking tonight?
- `Mr. Promise`: Sure thing!
- `Wendy`: While you are doing that, I will prepare fried tortillas 
  `(asynchronous operation)`. But make sure you let me know whether you could find sauce `(promise return value)`.
- `Mr. Promise`: What if you are not at home when I am back?
- `Wendy`: In that case, send me a text message saying you are back and have the 
  sauce for me `(success callback)`. If you don’t find it, call me immediately
  `(failure callback)`.
- `Mr. Promise`: Sounds good! See you in a bit.


You can check in simply terms that a `promise object` is data returned by `asynchronous function`. It can be a `resolve` if the function returned successfully or a `reject` if function returned an error.

## Promise Terminology

A `Promise` is an object representing the eventual `completion` or  `failure` of an
`asynchronous operation`. Essentially, a `promise` is a `returned object` you `attach` callbacks to, instead of `passing` callbacks into a function.

A promise has 3 states. They are:

1. `Pending`: awaiting promise response.
2. `Resolve`: promise has successfully returned.
3. `Reject`: failure occurred.

## Creating a Promise

In a typical web-app, you could be doing multiple `asynchronous operations`, such as fetching images, getting data from JSON endpoint, talking to an API, etc.

The constructor syntax for a promise object is:

```javascript
//ES5
var promise = new Promise(function(resolve, reject) {
  // executor (the producing code)
});
```
```javascript
//ES6
const promise = new Promise((resolve, reject) => {
  // executor (the producing code)
})
```

Here's how you create a promise:

### Example 1

```javascript
const promise = new Promise(function(resolve, reject) {
    // "Do a thing" (May take some time)
    const example = 8

    if (example === 8) {
        /*everything turned out fine*/
        resolve("Stuff worked!")
    } else {
        reject(Error("It broke"))
    }
})
```

Steps to create a promise:

1. The `promise constructor` takes one argument.
2. A `callback` with two parameters.
3. `Resolve` and `reject`. Do something within the `callback`, perhaps async, then 
   call `resolve` if everything worked, otherwise call `reject`.

## Consuming Promises

A promise can be used like this:

```javascript
// Promise defined
const promise = new Promise(function(resolve, reject) {
    // "Do a thing" (May take some time)
    const example = 8

    if (example === 8) {
        /*everything turned out fine*/
        resolve("Stuff worked!")
    } else {
        reject(Error("It broke"))
    }
})

// Consuming promise
promise
  .then((result) => {
      console.log(result)   // "Stuff worked!"
  }, (error) => {
      console.log(error)    // Error: "It broke"
  })

```

We can see two things by running the code above to consume the promise:

1. `.then()` takes two arguments, a callback for a `success case`, and another for 
   the `failure case`. Both are optional, so you can add a callback for the success or failure case only.
2. If `promise` was successful, a `resolve` will happen and the console will log 
   `"Stuff worked"`, otherwise `"It broke"`. That state between resolve and reject where a response hasn’t been received is `pending` state.

### Example 2

Here’s an example of a promise constructor and a simple executor function with “producing code” that takes time (via setTimeout):

```javascript
const promise = new Promise((resolve, reject) => {
  // the function is executed automatically when the promise is constructed

  // after 1 second signal that the job is done with the result "done"
  setTimeout(() => resolve("done"), 1000);
})

// Consuming promise
promise
  .then((result) => {
      console.log(result)
  }, (error) => {
      console.log(error)
  })
```

We can see two things by running the code above:

1. The executor is called automatically and immediately (by `new Promise`).

2. The executor receives two arguments: `resolve` and `reject`. These functions 
   are pre-defined by the JavaScript engine, so we don’t need to create them. We should only call one of them when ready.

3. After one second of “processing” the executor calls resolve(`"done"`) to 
   produce the result.

4. `.then()` takes two arguments, a callback for a success case, and another for 
   the failure case.

5. If promise was successful, a `resolve` will happen and the console will log 
   `"done"` 

That was an example of a successful job completion, a “fulfilled promise”.

### Example 3

Let's see an example of the executor rejecting the promise with an error:

```javascript
// Promise
const promise = new Promise((resolve, reject) => {
  // after 1 second signal that the job is finished with an error
  setTimeout(() => reject(new Error("Whoops!")), 1000);
})

// Consuming promise
promise
  .then((result) => {
      console.log(result)
  }, (error) => {
      console.log(error)
  })
```

After one second of “processing” the executor calls reject(`"Whoops!"`) to produce the result. The call to reject(...) moves the promise object to `rejected` state.

If promise wasn´t successful, a `reject` will happen and the console will log `"Whoops!"`.

## Chaining Promises

You can chain `.then()` together to transform values or run additional async actions one after another.

### Transforming Values

You can transform values simply by returning the new value:

```javascript
const promise = new Promise((resolve, reject) => {
  resolve("Hello")
})

promise
    .then((str) => {
      console.log(str)    //Hello
      return `${str} World!`
    })
    .then((str) => {
      console.log(str)    //Hello World!
    })
```

## Error Handling

As we saw earlier, `.then()` takes two arguments, one for success, one for failure (or fulfill and reject). Rejections happen when a `promise` is explicitly rejected, but also implicitly if an error is thrown in the `constructor callback`. 

We can use `.catch()` to display an error to the user:

```javascript
const jsonPromise = new Promise((resolve, reject) => {
  // JSON.parse throws an error if you feed it some
  // invalid JSON, so this implicitly rejects:
  resolve(JSON.parse("This ain't JSON"))
})

jsonPromise
    .then((data) => {
        // This never happens:
        console.log("It worked!", data)
    })
    .catch((err) => {
        // Instead, this happens:
        console.log("It Failed!", err)
    })

// It Failed! SyntaxError: Unexpected token T in JSON at position 0
```

> This means it's useful to do all your promise-related work inside the promise `constructor callback`, so errors are automatically caught and become rejections.

The same goes for errors thrown in `.then()` callbacks:

```javascript
const jsonPromise = new Promise((resolve, reject) => {
  // JSON.parse throws an error if you feed it some
  // invalid JSON, so this implicitly rejects:
  resolve("This ain't JSON")
})

jsonPromise
    .then(JSON.parse)
    .then((data) => {
        // This never happens:
        console.log("It worked!", data)
    })
    .catch((err) => {
        // Instead, this happens:
        console.log("It Failed!", err)
    })

// It Failed! SyntaxError: Unexpected token T in JSON at position 0
```

When the `promise` is resolved:

```javascript
const jsonPromise = new Promise((resolve, reject) => {
  // JSON.parse throws an error if you feed it some
  // invalid JSON, so this implicitly rejects:
  resolve('{"data": "This is a JSON"}')
})

jsonPromise
    .then(JSON.parse)
    .then((data) => {
        // This happens:
        console.log("It worked!", data)
    })
    .catch((err) => {
        // This doesn't happen:
        console.log("It Failed!", err)
    })

// It worked! { data: 'This is a JSON' }
```


# NodeJS Challenges (Instructions) - Solving Problems  

## setTimeout Problem using Chaining Promises

### Objectives

- To use `Promises`.
- To use `Chaining Promises`.
- To use `Error Handling`.

### Description

Our client `timingX, S.A.P.I de C.V.` has the next problem using `setTimeout` in Javascript:

```javascript
console.log("1")
setTimeout(() => console.log("2"), 3000)
console.log("3")
setTimeout(() => console.log("4"), 1000)
```

The output will be...

```
1
3
4
2
```

The client wonders why `4` is logged before `2`. The reason for that is
that even though `line 2` started executing before `line 4`, it did not start executing for `3000ms` and hence `4` is logged before `2` .

### Constrainsts

The software team has decided:

- To use `promises` to resolve this problem.
- To define a `delay()` function to manage the timing.
- To respect the timing already set by the client for every number showed.
- Don't hardcode input values ​​for chained promises.

## Deliverables

Check final result. You'll have in terminal…

```
1
2
3
4
```

- When you run 'npm test'...

You'll have in terminal…

```
$ npm i
$ npm test

> jest

 PASS  tests/promiseTiming.test.js (8.549 s)
  delay promise
    √ should be an entry numeric string (9 ms)
    √ should get numeric string "2" from data input (3007 ms)
    √ should return a numeric string from entry number (1004 ms)
    √ should sum values from input data (3023 ms)
    √ should fail the input string with an error (1015 ms)
    √ should fail any input with an error (2 ms)

Test Suites: 1 passed, 1 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        8.678 s, estimated 9 s
Ran all test suites.

```

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


> References:

1. [Consuming Promises](https://www.i-programmer.info/programming/javascript/12180-javascript-async-consuming-promises.html)

2. [Understanding Javascript Promises](https://www.digitalocean.com/community/tutorials/understanding-javascript-promises)

3. [Promise Basics](https://javascript.info/promise-basics)